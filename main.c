#include <stdio.h>                                  
#include <stdint.h>                                 
#include <stdlib.h>                                 
#include <time.h>                                   
                                                    
static uint16_t                                     
prim(uint16_t a, uint16_t b)                        
{                                                   
        static const uint_fast16_t c[] = { 1, 0 };  
        a = a + c[a & 1];                           
        b = b - c[b & 1];                           
                                                    
        for (; a <= b; ++a) {                       
                for (uint16_t i = 3; i < a / 2; ++i)
                        if (a % i == 0)             
                                goto _PRIM_CONT_;   
                }                                   
                                                    
                return a;                           
_PRIM_CONT_:                                        
                continue;                           
        }                                           
                                                    
        return 0;                                   
}                                                   
                                                    
static uint16_t                                     
sqml(uint16_t x, uint16_t k, uint16_t p)            
{                                                   
        int c;                                      
        c  = __builtin_clz(k);                      
        c -= 16;                                    
                                                    
        k <<= c + 1;                                
                                                    
        uint_fast32_t r;                            
        r = x;                                      
        for (int i = c + 1; i < 16; ++i) {          
                r = (r * r) % p;                    
                if (k & 0x8000)                     
                        r = (r * x) % p;            
                k <<= 1;                                  
        }                                                 
                                                          
        return r;                                         
}                                                         
                                                          
static uint16_t                                           
gen(uint16_t p)                                           
{                                                         
        // Z_p* = { 1, 2, ..., p - 1 }                    
        // g => |Z_p*| = len(g)                           
                                                          
        for (uint16_t i = 2, j; i < p; ++i) {             
                for (j = 2; j < p; ++j)                   
                        if (sqml(i, j, p) == 1)           
                                break;                    
                                                          
                if (j == p - 1)                           
                        return i;                         
        }                                                 
                                                          
        return 0;                                         
}                                                         
                                                          
static uint16_t                                           
pick(uint16_t a, uint16_t b)                              
{                                                         
        uint16_t r;                                       
        // r in [a, b]                                    
        r = rand() % (b + 1 - a) + a;                     
        return r;                                         
}                                                         
                                                          
int                                                       
main(void)                                                
{                                                         
        // ---------- ALICE ------------>                 
        uint16_t x;                                       
        x = 26;                                           
        printf("x: %u\n", x);                             
        // <---------- ALICE ------------    
                     
        // ---------- BOB ------------>            
        uint16_t p;                                
        p = prim(29, 31);                          
        printf("p: %u\n", p);                      
                                                    
        uint16_t alpha;                            
        alpha = gen(p);                            
        printf("alpha: %u\n", alpha);              
                                                    
        uint16_t d;                                
        //d = pick(2, p - 2);                      
        d = 12;                                    
        printf("d: %u\n", d);                      
                                                    
        uint16_t beta;                             
        beta = sqml(alpha, d, p);                  
        printf("beta: %u\n", beta);                
        // <---------- BOB ------------            
                                                    
        // ---------- ALICE ------------>          
        uint16_t i;                                
        //i = pick(2, p - 2);                      
        i = 5;                                     
        printf("i: %u\n", i);                      
                                                    
        uint16_t k_e, k_m;                         
        k_e = sqml(alpha, i, p);                   
        k_m = sqml(beta, i, p);                    
        printf("k_e: %u, k_m: %u\n", k_e, k_m);    
                                                    
        uint16_t y;                                
        y = (x * k_m) % p;                         
        printf("y: %u\n", y);                      
        // <---------- ALICE ------------          
                                                    
        // ---------- BOB ------------>            
        uint16_t k_d;                              
        k_d = sqml(k_e, p - d - 1, p);             
        printf("k_d: %u\n", k_d);                          
                                            
        uint16_t x_r;                      
        x_r = (y * k_d) % p;               
        printf("x_r: %u\n", x_r);          
        // <---------- BOB ------------    
                                            
        return 0;                          
}                                                                                      
